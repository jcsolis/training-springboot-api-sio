package com.dca.sio.app.user.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication
@EntityScan({"com.dca.sio.app.users.commons.model.entity"})
public class SpringbootSioApiUserApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootSioApiUserApplication.class, args);
    }

}
