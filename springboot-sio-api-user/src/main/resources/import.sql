INSERT INTO users(username, password, name, lastname, email, enabled) VALUES('jcsolis','$2a$10$YNqJ39zm4KjNS6pBLgxQx.l5PHeZihsJzag7gipPNVeWrAsol11vK','Jhon','Solis','jcsolis78@gmail.com',1);
INSERT INTO users(username, password, name, lastname, email, enabled) VALUES('asolis','$2a$10$iNwwPEWXt6dy5h/niJejSep9ibRWChe6e2/vjEAaajLhwb.dPO2My','Andrea','Solis','andrea.solis@gmail.com',1);

INSERT INTO roles(name) VALUES('ROLE_USER');
INSERT INTO roles(name) VALUES('ROLE_ADMIN');


INSERT INTO users_roles(user_id, role_id) VALUES(1, 1);
INSERT INTO users_roles(user_id, role_id) VALUES(2, 2);
INSERT INTO users_roles(user_id, role_id) VALUES(2, 1);