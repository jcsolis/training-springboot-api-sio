package com.dca.sio.app.oauth.server.clientConfig;

import com.dca.sio.app.users.commons.model.entity.User;

public interface IUserService {
    User findByUsername(String username);
    User update(User user, Long id);

}
