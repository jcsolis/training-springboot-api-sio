package com.dca.sio.app.oauth.server.security.event;

import brave.Tracer;
import com.dca.sio.app.oauth.server.clientConfig.IUserService;
import com.dca.sio.app.users.commons.model.entity.User;
import feign.FeignException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationEventPublisher;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

@Component
public class AuthenticationSuccessErrorHandler implements AuthenticationEventPublisher {

    private Logger log = LoggerFactory.getLogger(AuthenticationSuccessErrorHandler.class);

    @Autowired
    private IUserService userService;


    @Autowired
    private Tracer tracer;

    @Override
    public void publishAuthenticationSuccess(Authentication authentication) {
        UserDetails user = (UserDetails)authentication.getPrincipal();
        String messsage = "Autenticado con exito: " + user.getUsername();
        System.out.println(messsage);
        log.info(messsage);

        User userOk = userService.findByUsername(authentication.getName());

        if(userOk.getIntents() != null && userOk.getIntents() > 0){
            userOk.setIntents(0);
            userService.update(userOk, userOk.getId());
        }

    }

    @Override
    public void publishAuthenticationFailure(AuthenticationException exception, Authentication authentication) {
        String messsage = "Error autenticando: " + exception.getMessage();
        log.error(messsage);
        System.out.println(messsage);

        try {

            StringBuilder errors = new StringBuilder();
            errors.append(messsage);

            User user = userService.findByUsername(authentication.getName());

            if(user.getIntents() == null){
                user.setIntents(0);
            }
            log.info("Numero de intentos: " + user.getIntents());
            user.setIntents(user.getIntents() + 1);

            errors.append(" - Intentos de autenticación: " + user.getIntents());

            if(user.getIntents() >= 3){
                String maxIntents = String.format("El usuario %s ha sido deshabilitado por número maxímo de intentos", user.getUsername());
                errors.append(" - " + maxIntents);
                log.error(maxIntents);
                user.setEnabled(false);
            }

            userService.update(user, user.getId());

            tracer.currentSpan().tag("error.message",errors.toString());

        }catch (FeignException e){
            log.error(String.format("El usuario %s no esta en el sistema", authentication.getName()));
        }


    }
}
