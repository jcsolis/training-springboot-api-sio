package com.dca.sio.app.oauth.server.clientConfig;

import com.dca.sio.app.users.commons.model.entity.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient(name = "user-service")
public interface IConfigClient {

    @GetMapping("/users/search/findUsername")
    User findByUsername(@RequestParam("username") String username);

    @PutMapping("/users/{id}")
    User update(@RequestBody User user, @PathVariable Long id);
}
