package com.dca.sio.app.oauth.server.services;

import brave.Tracer;
import com.dca.sio.app.oauth.server.clientConfig.IConfigClient;
import com.dca.sio.app.oauth.server.clientConfig.IUserService;
import com.dca.sio.app.users.commons.model.entity.User;
import feign.FeignException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService implements IUserService, UserDetailsService {

    private Logger log = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private Tracer tracer;

    @Autowired
    private IConfigClient configClient;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        try {

            User user = configClient.findByUsername(username);

            List<GrantedAuthority> authorities = user.getRoles()
                    .stream()
                    .map(role -> new SimpleGrantedAuthority(role.getName()))
                    .peek(role -> log.info("Role: " + role.getAuthority()))
                    .collect(Collectors.toList());

            log.info("Usuario autenticado: " + username);


            return new org.springframework.security.core.userdetails.User(user.getUsername(),
                    user.getPassword(), user.getEnabled(), true,
                    true, true, authorities);

        }catch (FeignException e){
            String error = "Error de acceso, el usuario " + username + " no esta registrado en el sistema.";

            tracer.currentSpan().tag("error.message",error + ": " + e.getMessage());

            log.error(error);
            throw new UsernameNotFoundException(error);
        }
    }

    @Override
    public User findByUsername(String username) {
        User user = configClient.findByUsername(username);
        log.info("Usuario " + username + " es " + user.getName() + " " + user.getLastname()  );
        return user;
    }

    @Override
    public User update(User user, Long id) {
        return configClient.update(user,id);
    }
}
