package com.dca.sio.app.product.model.service;

import com.dca.sio.app.product.model.entity.Product;

import java.util.List;

public interface IProductService {

    List<Product> findAll();

    Product findById(Long id);

    Product create(Product product);

    Product edit(Product product, Long id) throws Exception;

    void delete(Long id);
}
