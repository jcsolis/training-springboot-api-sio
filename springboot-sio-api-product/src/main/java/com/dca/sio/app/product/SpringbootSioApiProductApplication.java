package com.dca.sio.app.product;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class SpringbootSioApiProductApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootSioApiProductApplication.class, args);
    }

}
