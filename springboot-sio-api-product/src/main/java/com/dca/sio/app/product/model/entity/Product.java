package com.dca.sio.app.product.model.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "products")
@Data
public class Product implements Serializable {

    private static final long serialVersionUID = 5929576005834590894L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private Double prize;

    @Transient
    private Integer port;

    @Column(name = "create_date")
    @Temporal(TemporalType.DATE)
    private Date createDate;

}
