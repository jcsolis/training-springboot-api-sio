package com.dca.sio.app.product.model.dao;

import com.dca.sio.app.product.model.entity.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IProductDao extends CrudRepository<Product, Long> {
}
