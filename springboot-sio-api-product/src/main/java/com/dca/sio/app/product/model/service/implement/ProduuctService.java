package com.dca.sio.app.product.model.service.implement;

import com.dca.sio.app.product.model.dao.IProductDao;
import com.dca.sio.app.product.model.entity.Product;
import com.dca.sio.app.product.model.service.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ProduuctService implements IProductService {


    @Autowired
    private IProductDao productDao;

    @Override
    @Transactional(readOnly = true)
    public List<Product> findAll() {
        return (List<Product>) productDao.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Product findById(Long id) {
        return productDao.findById(id).orElse(null);
    }

    @Override
    @Transactional
    public Product create(Product product) {
        return productDao.save(product);
    }

    @Override
    @Transactional
    public Product edit(Product product, Long id) throws Exception {

        Product getProduct = findById(id);

        if (getProduct == null) {
            throw new Exception("El ID " + id.toString() + " no esta disponible");
        }

        getProduct.setName(product.getName());
        getProduct.setPrize(product.getPrize());


        return productDao.save(getProduct);

    }

    @Override
    @Transactional
    public void delete(Long id) {
        productDao.deleteById(id);
    }
}
