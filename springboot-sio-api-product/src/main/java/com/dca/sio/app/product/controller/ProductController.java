package com.dca.sio.app.product.controller;

import com.dca.sio.app.product.model.entity.Product;
import com.dca.sio.app.product.model.service.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class ProductController {

    @Autowired
    private Environment environment;

    @Autowired
    private IProductService productService;

    @GetMapping("/listar")
    public List<Product> listProduct() {
        return productService.findAll().stream().map(p -> {
            p.setPort(Integer.parseInt(environment.getProperty("local.server.port")));
            return p;
        }).collect(Collectors.toList());
    }

    @GetMapping("/ver/{id}")
    public Product listProduct(@PathVariable Long id) {
        Product product = productService.findById(id);
        product.setPort(Integer.parseInt(environment.getProperty("local.server.port")));

        /*Boolean err = false;
        if (!err){
            throw new Exception("Hay un error en la ejecución");
        }

        try {
            Thread.sleep(2000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/

        return product;
    }

    @PostMapping("/create")
    public Product create(@RequestBody Product product) {
        return productService.create(product);
    }

    @PutMapping("/edit/{id}")
    public Product edit(@RequestBody Product product, @PathVariable Long id) throws Exception {
        return productService.edit(product, id);
    }

    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable Long id) {
        productService.delete(id);
    }


}
