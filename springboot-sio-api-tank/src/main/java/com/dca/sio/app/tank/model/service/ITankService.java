package com.dca.sio.app.tank.model.service;

import com.dca.sio.app.tank.model.entity.Tank;

import java.util.List;

public interface ITankService {
    List<Tank> findAll();

    Tank findById(Long id, Integer quantity);
}
