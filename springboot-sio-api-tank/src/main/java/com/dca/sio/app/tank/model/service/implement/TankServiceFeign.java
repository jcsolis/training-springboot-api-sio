package com.dca.sio.app.tank.model.service.implement;

import com.dca.sio.app.tank.clients.ProductClientRest;
import com.dca.sio.app.tank.model.entity.Tank;
import com.dca.sio.app.tank.model.service.ITankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service("serviceFeign")
public class TankServiceFeign implements ITankService {

    @Autowired
    private ProductClientRest clientRest;

    @Override
    public List<Tank> findAll() {
        return clientRest.listProduct().stream().map(p -> new Tank(p, 1)).collect(Collectors.toList());
    }

    @Override
    public Tank findById(Long id, Integer quantity) {
        return new Tank(clientRest.listProduct(id), quantity);
    }
}
