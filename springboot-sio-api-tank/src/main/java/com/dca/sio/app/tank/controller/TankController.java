package com.dca.sio.app.tank.controller;

import com.dca.sio.app.tank.model.entity.Product;
import com.dca.sio.app.tank.model.entity.Tank;
import com.dca.sio.app.tank.model.service.ITankService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RefreshScope
@RestController
public class TankController {

    @Value("${config-text}")
    private String configText;

    @Value("${server.port}")
    private String serverPort;

    @Autowired
    private Environment environment;

    @Autowired
    @Qualifier("serviceFeign")
    private ITankService tankService;

    @GetMapping("/listar")
    public List<Tank> findAll() {
        return tankService.findAll();
    }

    @HystrixCommand(fallbackMethod = "alternateDetail")
    @GetMapping("/ver/{id}/cantidad/{quantity}")
    public Tank details(@PathVariable Long id, @PathVariable Integer quantity) {
        return tankService.findById(id, quantity);
    }

    public Tank alternateDetail(Long id, Integer quantity) {
        Tank tank = new Tank();
        Product product = new Product();

        tank.setQuantity(quantity);
        product.setId(id);
        product.setName("PRODUCTO ALTERNO");
        product.setPrize(526.0);
        tank.setProduct(product);

        return tank;
    }

    @GetMapping("/obtener-config")
    public ResponseEntity<?> obtenerConfig(){
        Map<String, String> json = new HashMap<>();
        json.put("text", configText);
        json.put("port",serverPort);

        if(environment.getActiveProfiles().length > 0 && environment.getActiveProfiles()[0].equals("dev")){
            json.put("nombre", environment.getProperty("configuracion.autor.nombre"));
            json.put("correo", environment.getProperty("configuracion.autor.email"));
        }
        return new ResponseEntity<Map<String, String>>(json, HttpStatus.OK);
    }

}
