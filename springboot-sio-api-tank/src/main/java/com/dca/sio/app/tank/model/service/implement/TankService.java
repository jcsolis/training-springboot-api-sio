package com.dca.sio.app.tank.model.service.implement;

import com.dca.sio.app.tank.model.entity.Product;
import com.dca.sio.app.tank.model.entity.Tank;
import com.dca.sio.app.tank.model.service.ITankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service("serviceRestTemplate")
public class TankService implements ITankService {

    @Autowired
    private RestTemplate restTemplate;

    private List<Product> getProduct() {
        return Arrays.asList(
                restTemplate.getForObject("http://localhost:8001/listar", Product[].class));

    }

    @Override
    public List<Tank> findAll() {

        List<Product> products = getProduct();

        return products.stream().map(p -> new Tank(p, 1)).collect(Collectors.toList());
    }

    @Override
    public Tank findById(Long id, Integer quantity) {
        Map<String, String> pathVariables = new HashMap<String, String>();
        pathVariables.put("id", id.toString());
        Product product = restTemplate.getForObject("http://localhost:8001/ver/{id}", Product.class, pathVariables);
        return new Tank(product, quantity);
    }
}
