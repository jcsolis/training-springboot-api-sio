package com.dca.sio.app.tank.clients;

import com.dca.sio.app.tank.model.entity.Product;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(name = "product-service") //, url = "http://localhost:8001")
public interface ProductClientRest {

    @GetMapping("/listar")
    public List<Product> listProduct();

    @GetMapping("/ver/{id}")
    public Product listProduct(@PathVariable Long id);
}
