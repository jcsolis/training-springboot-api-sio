package com.dca.sio.app.tank.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Tank {

    private Product product;
    private Integer quantity;

    public Double getTotal() {
        return product.getPrize() * quantity.doubleValue();
    }

}
